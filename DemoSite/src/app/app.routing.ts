import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SiteMainComponent } from '../app/site-master/site-main/site-main.component';
import { LandListComponent } from '../app/r6-land/land-list/land-list.component';

const appRoutes : Routes = [
  { path: 'site-home', component: SiteMainComponent },
  {
    path: '',
    redirectTo: 'site-home',
    pathMatch: 'full'
  },
  { path: '**', component: SiteMainComponent },
];

export const appRoutingProviders: any[] = [

];

export const appRouting: ModuleWithProviders = RouterModule.forRoot(appRoutes);