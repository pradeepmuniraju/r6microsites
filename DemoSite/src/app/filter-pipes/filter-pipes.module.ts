import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LotlistFilterPipe } from './lotlist-filter.pipe';
import { HandllistFilterPipe } from './handllist-filter.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [LotlistFilterPipe, HandllistFilterPipe],
  exports: [LotlistFilterPipe, HandllistFilterPipe]
})
export class FilterPipesModule { }
