export class LotCriteria {
    showAll: string;
    lotnumber: string;
    status: string;
    lotwidth: number;
    lotdepth: number;
    areaFrom: number;
    areaTo: number;
    priceFrom: number;
    priceTo: number;

}
