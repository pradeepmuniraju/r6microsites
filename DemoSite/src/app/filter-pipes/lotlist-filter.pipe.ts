import { Pipe, PipeTransform } from '@angular/core';

import { LotCriteria } from './lotcriteria.model';

@Pipe({
  name: 'lotlistFilter'
})
export class LotlistFilterPipe implements PipeTransform {

  transform(lostList: any, filterCriteria: LotCriteria): any {
    if (filterCriteria === undefined) return lostList;
    if (filterCriteria.showAll === 'All') return lostList;

    let filteredLotList: any;
    return lostList.filter(function(lot){
      filteredLotList = lostList;
      if (!filterCriteria.lotnumber === undefined) {
        filteredLotList = lot.productname.toLowerCase().includes(filterCriteria.lotnumber.toLowerCase());
      }
      if (!filterCriteria.status === undefined) {
        filteredLotList = lot.currentstatusname.toLowerCase().includes(filterCriteria.status.toLowerCase());
      }
      if (!filterCriteria.areaFrom === undefined) {
        let number1 : number;
        if (number1 > filterCriteria.areaFrom) {

        }
        filteredLotList = lot.produtsize.toLowerCase().includes(filterCriteria.status.toLowerCase());
      }
      return filteredLotList;
    })
  }

}
