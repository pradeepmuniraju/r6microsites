import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { R6MaterialModule } from '../r6-material/r6-material.module';
import { R6SharedModule } from '../r6-shared/r6-shared.module';

import { RegisterFormComponent } from './register-form/register-form.component';

@NgModule({
  imports: [
    CommonModule,
    R6MaterialModule,
    R6SharedModule
  ],
  declarations: [RegisterFormComponent],
  exports: [RegisterFormComponent]
})
export class R6FormsModule { }
