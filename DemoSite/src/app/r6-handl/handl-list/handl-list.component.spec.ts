import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandlListComponent } from './handl-list.component';

describe('HandlListComponent', () => {
  let component: HandlListComponent;
  let fixture: ComponentFixture<HandlListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandlListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandlListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
