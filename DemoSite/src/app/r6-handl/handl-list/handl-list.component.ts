import { Component, OnInit, Input } from '@angular/core';

import { Handlsummary } from  '../handlsummary.model';
import { R6HandlService } from  '../r6-handl.service';

@Component({
  selector: 'app-handl-list',
  templateUrl: './handl-list.component.html',
  styleUrls: ['./handl-list.component.css']
})
export class HandlListComponent implements OnInit {

  @Input() estateCpId: string;
 
  handlList: Handlsummary[];

  constructor(private r6handlService: R6HandlService) { }

  ngOnInit() {
    this.listHandLSummary();
  }

  listHandLSummary() {
    this.handlList = [];
    this.r6handlService.listHandLSummaryByEstate(this.estateCpId)
      .subscribe(handlList => this.handlList = handlList);
  }

}
