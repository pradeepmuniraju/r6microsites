import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { R6SharedModule } from '../r6-shared/r6-shared.module';

import { HandlListComponent } from './handl-list/handl-list.component';
import { R6HandlService } from './r6-handl.service';

@NgModule({
  imports: [
    CommonModule,
    R6SharedModule
  ],
  declarations: [ HandlListComponent ],
  exports: [ HandlListComponent ],
  providers: [ R6HandlService ]
})
export class R6HandlModule { }
