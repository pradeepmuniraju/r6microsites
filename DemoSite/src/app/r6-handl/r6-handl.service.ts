import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from "rxjs/Observable";

import { REST_API_SERVICE_URL } from "../r6-shared/apiurl-constants";
import { R6ApiService } from "../r6-shared/r6api.service";

import { Handlsummary } from  './handlsummary.model';

@Injectable()
export class R6HandlService {

  private HANDL_SUMMARY_LIST_BYESTATE_API: string = REST_API_SERVICE_URL + "/houseandlandapi/houseandlandsummary/list?estatecpid=";

  constructor(private r6apiService: R6ApiService) { }

  listHandLSummaryByEstate(estatecpId: string): Observable<Handlsummary[]> {
    console.log(this.HANDL_SUMMARY_LIST_BYESTATE_API);
    let listPackageByEstate = this.HANDL_SUMMARY_LIST_BYESTATE_API + estatecpId;

    return this.r6apiService.get(listPackageByEstate)
       .map((response: Response) => <Handlsummary[]>response.json());
    
  }

}
