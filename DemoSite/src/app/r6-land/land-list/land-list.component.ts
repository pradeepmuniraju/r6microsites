import { Component, OnInit, Input } from '@angular/core';

import { Lotsummary } from  '../lotsummary.model';
import { R6LandService } from  '../r6-land.service';

import { LotCriteria } from  '../../filter-pipes/lotcriteria.model';

@Component({
  selector: 'app-land-list',
  templateUrl: './land-list.component.html',
  styleUrls: ['./land-list.component.css']
})
export class LandListComponent implements OnInit {

  @Input() estateCpId: string;

  lotCriteria: LotCriteria;
  lotList: Lotsummary[];
  lotNumberList: string[];
  statusList: string[];

  selectedLot: string;
  selectedStatus: string;

  constructor(private r6LandService: R6LandService) { 
    this.lotCriteria = new LotCriteria;
    this.lotCriteria.showAll = 'All';
  }

  ngOnInit() {
    this.statusList = [];
    this.statusList.push('Available');
    this.statusList.push('Reserved');
    this.statusList.push('Sold');

    this.listLotSummary();
  }

  listLotSummary() {
    let estatecpid = '6567422136284545060773548341680648211473120049451';
    //this.lotList = [];
    //this.lotNumberList = [];
    this.r6LandService.listLotSummaryByEstate(this.estateCpId)
      .subscribe(lotList => this.setLotListSummaryResponse(lotList));
  }

  setLotListSummaryResponse(lotList: Lotsummary[]) {
    this.lotList = lotList;
    this.lotNumberList = [];
    for (var i=0; i<this.lotList.length; i++) {
      this.lotNumberList.push(this.lotList[i].productname);
    }

  }
}
