import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { R6SharedModule } from '../r6-shared/r6-shared.module';
import { FilterPipesModule } from '../filter-pipes/filter-pipes.module';

import { LandListComponent } from './land-list/land-list.component';
import { R6LandService } from './r6-land.service';

@NgModule({
  imports: [
    CommonModule,
    R6SharedModule,
    FilterPipesModule
  ],
  declarations: [ LandListComponent ],
  exports: [ LandListComponent ],
  providers: [ R6LandService ]
})
export class R6LandModule { }
