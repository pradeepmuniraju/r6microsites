import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from "rxjs/Observable";

import { REST_API_SERVICE_URL } from "../r6-shared/apiurl-constants";
import { R6ApiService } from "../r6-shared/r6api.service";

import { Lotsummary } from  './lotsummary.model';

@Injectable()
export class R6LandService {

  private ESTATE_SUMMARY_LIST_API: string = REST_API_SERVICE_URL + "/landapi/estatesummary/list";
  private LOT_SUMMARY_LIST_API: string = REST_API_SERVICE_URL + "/landapi/landsummary/list";
  private LOT_SUMMARY_LIST_BYESTATE_API: string = REST_API_SERVICE_URL + '/landapi/landsummary/list?estatecpid=';

  constructor(private r6apiService: R6ApiService) { }

  listLotSummaryByEstate(estatecpId: string): Observable<Lotsummary[]> {
    
    let listLotsByEstate = this.LOT_SUMMARY_LIST_API + '?estatecpid=' + estatecpId;
    console.log(listLotsByEstate);

    return this.r6apiService.get(listLotsByEstate)
       .map((response: Response) => <Lotsummary[]>response.json());
    
  }

}
