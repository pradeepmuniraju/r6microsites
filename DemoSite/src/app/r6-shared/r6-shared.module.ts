import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { R6ApiService } from './r6api.service';
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers : [R6ApiService]
})
export class R6SharedModule { }
