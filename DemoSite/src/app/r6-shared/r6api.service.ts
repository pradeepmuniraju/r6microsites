import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class R6ApiService {

  r6AuthApiKey: string;
  securitytoken: string;

  constructor(private http:Http) { 
    this.r6AuthApiKey = 'Basic cGlwZWxpbmUyd3M6VzNsY29tZSEyMw==';
    
    this.securitytoken = '8407503453665001131154183167530314331494039484686';
  }

  setAuthorisationHeaders(headers:Headers) {
    headers.append('Authorization', this.r6AuthApiKey);
  }

  get(url: string) : Observable<Response> {
    let headers = new Headers();
    console.log(url);

    //this.securitytoken = localStorage.getItem('R6SECURITYTOKEN');

    console.log("Security Token : " + this.securitytoken);
    headers.append('Authorization', this.r6AuthApiKey);
    headers.append('Content-Type', 'application/json');
    headers.append('securitytoken', this.securitytoken);

    return this.http.get(url, {headers});

  }

  post(url: string, data: any) : Observable<Response> {
    let headers = new Headers();
    console.log("URL Is   " + url +" Data is "+data);
    //this.securitytoken = localStorage.getItem('R6SECURITYTOKEN');
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.r6AuthApiKey);
    headers.append('securitytoken', this.securitytoken);

    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, data, options);
  }


}
