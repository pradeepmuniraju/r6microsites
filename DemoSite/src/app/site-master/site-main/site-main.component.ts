import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { MdDialog, MdDialogConfig, MdDialogRef } from '@angular/material';

import { RegisterFormComponent } from '../../r6-forms/register-form/register-form.component';

@Component({
  selector: 'app-site-main',
  templateUrl: './site-main.component.html',
  styleUrls: ['./site-main.component.css']
})
export class SiteMainComponent implements OnInit {

  dialogRef : MdDialogRef<any>;
  estateCpId : string =  '6567422136284545060773548341680648211473120049451';

  constructor(public mdDialog : MdDialog,
              public vcr : ViewContainerRef) { 
                this.estateCpId = '6567422136284545060773548341680648211473120049451';
              }

  ngOnInit() {
  }


  registerButtonClick() {
    const config = new MdDialogConfig();
    config.viewContainerRef = this.vcr;
    this.dialogRef = this.mdDialog.open(RegisterFormComponent, config);

    this.dialogRef.afterClosed().subscribe(result => {
      this.dialogRef = null;
      
    })
  }

}
