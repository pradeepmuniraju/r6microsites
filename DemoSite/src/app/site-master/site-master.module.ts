import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { R6MaterialModule } from '../r6-material/r6-material.module';
import { R6SharedModule } from '../r6-shared/r6-shared.module';

import { R6LandModule } from '../r6-land/r6-land.module';
import { R6HandlModule } from '../r6-handl/r6-handl.module';
import { R6FormsModule } from '../r6-forms/r6-forms.module';

import { SiteMainComponent } from './site-main/site-main.component';
import { RegisterFormComponent } from '../r6-forms/register-form/register-form.component';

@NgModule({
  imports: [
    CommonModule,
    R6MaterialModule,
    R6SharedModule,
    R6LandModule,
    R6HandlModule,
    R6FormsModule
  ],
  entryComponents: [RegisterFormComponent],
  declarations: [ SiteMainComponent ],
  exports: [ SiteMainComponent ]
})
export class SiteMasterModule { }
